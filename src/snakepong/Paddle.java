package snakepong;

import java.awt.Color;
import java.awt.Graphics;

public class Paddle {
	double y, yVel;
	final double FRICTION = 0.92;
	boolean up, down;
	
	public Paddle() {
		up = false; down = false;
		y = 210; yVel = 0;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.yellow);
		g.fillRect(20, (int)y, 20, 100);
	}
	
	public void move() {
		if(up) {
			yVel -= 1;
		}
		else if(down) {
			yVel += 1;
		}
		else if(!up && !down) {
			yVel *= FRICTION;
		}
		if(yVel >= 5) {
			yVel = 5;
		}
		else if(yVel <= -5)  {
			yVel = -5;
		}
		y += yVel;
		
		if(y < 0) {
			y = 0;
		}
		
		if(y > Core.HEIGHT - 100 ) {
			y = Core.HEIGHT - 100;
		}
	} 
	
	public void setUp(boolean input) {
		up = input;
	}
	
	public void setDown(boolean input) {
		down = input;
	}
	
	public int getY() {
		return (int)y;
	}
}
