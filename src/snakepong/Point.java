package snakepong;

public class Point {
	private int xCo, yCo;
	
	public Point(int x, int y) {
		xCo = x;
		yCo = y;
	}
	
	public int getX() {
		return xCo;
	}
	
	public int getY() {
		return yCo;
	}
	
	public void setX(int x) {
		xCo = x;
	}
	
	public void setY(int y) {
		yCo = y;
	}
}
