package snakepong;

import java.awt.Color;
import java.awt.Graphics;

public class Ball {
	double xVel, yVel, x, y;
	int score;
	
	public Ball() {
		x = 90;
		y = 90;
		xVel = 4;
		yVel = 3;
		score = 0;
	}
	
	public void checkPaddleCollision(Paddle p) {
		if(x == 50) {
			if(y >= p.getY() && y <= p.getY() + 100) {
				xVel = -xVel;
			}
		}
	}
	
	public void checkSnakeCollision(Snake s) {
		for(Point p : s.snakePoints) {
			if(p.getX() + 20 >= x && p.getX() - 20 <= x && p.getY() + 10 > y && p.getY() - 10 <= y) {
				xVel = -xVel;
			}
			if(p.getX() + 10 > x && p.getX() - 10 <= x && p.getY() + 20 >= y && p.getY() - 20 <= y) {
				yVel = -yVel;
			}
		}
	}
	
	public void checkWallCollision(Snake s) {
		if(x <= -10) {
			 x = 90; y = 90; xVel = -xVel;
			 Point last = s.snakePoints.getLast();
			 for(int i = 1; i < 6; i++) {
					s.snakePoints.append(new Point(last.getX() - i * 4, last.getY()));
				}
			 score++;
		}
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.blue);
		g.fillOval((int)x-10, (int)y-10, 20, 20);
		g.setColor(Color.pink);
		g.drawString("Score: " +score, 10, 20);
	}
	
	public void move() {
		x += xVel;
		y += yVel;
		
		if(x > Core.WIDTH - 10) {
			xVel = -xVel;
		}
		
		if(y < 10) {
			yVel = -yVel;
		}
		if(y > Core.HEIGHT - 10) {
			yVel = -yVel;
		}
	}
	
	public int getX() {
		return (int)x;
	}
	
	public int getY() {
		return (int)y;
	}
	
}
