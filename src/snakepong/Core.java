package snakepong;

import javax.swing.JApplet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Core extends JApplet implements Runnable, KeyListener {
	
	final static Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	final static int WIDTH = dim.width/2;
	final static int HEIGHT = dim.height/2;
	boolean gameOver;
	int score;
	Graphics gfx;
	Image img;
	Thread thread;
	Paddle p;
	Ball b;
	Snake s;
	
	public void init() {
		this.resize(WIDTH, HEIGHT);
		gameOver = false;
		score = 0;
		img = createImage(WIDTH, HEIGHT);
		gfx = img.getGraphics();
		p = new Paddle();
		b = new Ball();
		s = new Snake();
		thread = new Thread(this);
		thread.start();
		this.addKeyListener(this);
		setFocusable(true);
	}
	
	public void paint(Graphics g) {
		gfx.setColor(Color.black);
		gfx.fillRect(0, 0, WIDTH, HEIGHT);
		gfx.setColor(Color.blue);
		gfx.fillRect(0, 0, 2, HEIGHT);
		gfx.setColor(Color.red);
		gfx.fillRect(WIDTH-3, 0, 2, HEIGHT);
		gfx.fillRect(0, 0, WIDTH, 2);
		gfx.fillRect(0, HEIGHT-3, WIDTH, 2);
		if(!gameOver) {
			p.draw(gfx);
			b.draw(gfx);
			s.draw(gfx);
		}
		else {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			gfx.setColor(Color.RED);
			gfx.drawString("Game Over", WIDTH/2 - 40, HEIGHT/2);
			gfx.drawString("Total score: " + b.score, WIDTH/2 - 40, HEIGHT/2 + 20);
		}
		
		g.drawImage(img, 0, 0, null);
	}
	
	public void update(Graphics g) {
		paint(g);
	}
	
	public void repaint(Graphics g) {
		paint(g);
	}
	
	public void run() {
		for(;;) {
			
			if(!gameOver) {
				p.move();
				s.move();
				b.move();
				b.checkPaddleCollision(p);
				b.checkSnakeCollision(s);
				b.checkWallCollision(s);
				this.checkGameOver();
			}
			
			this.repaint();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void checkGameOver() {
		if(s.getX() < 10 || s.getX() > WIDTH - 10) {
			gameOver = true;
		}
		if(s.getY() < 10 || s.getY() > HEIGHT - 10) {
			gameOver = true;
		}
		if(s.snakeCollision()) {
			gameOver = true;
		}
	}

	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_Z) {
			p.setUp(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_S) {
			p.setDown(true);
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			if(s.getYdir() != 1) {
				s.setYDir(-1);
				s.setXDir(0);
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			if(s.getYdir() != -1) {
				s.setYDir(1);
				s.setXDir(0);
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			if(s.getXdir() != 1) {
				s.setXDir(-1);
				s.setYDir(0);
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if(s.getXdir() != -1) {
				s.setXDir(1);
				s.setYDir(0);
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_Z) {
			p.setUp(false);
		}
		if(e.getKeyCode() == KeyEvent.VK_S) {
			p.setDown(false);
		}
	}

	public void keyTyped(KeyEvent e) {
	}
}
