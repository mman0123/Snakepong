package snakepong;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> implements Iterable<E> {
	private int size;
	private Node head;
	private Node tail;
	
	public DoublyLinkedList() {
		size = 0;
		head = null;
		tail = null;
	}
	
	private DoublyLinkedList(Node node) {
		head = node;
		size = count(node);
	}
	
	private int count(Node node) {
		return countR(node, 0);
	}
	
	private int countR(Node cursor, int count) {
		if(cursor == null) return count;
		return countR(cursor.getNext(), count+1);
	}
	
	public DoublyLinkedList<E> getTail(){
		return new DoublyLinkedList<E>(head.getNext());
	}
	
	public E getLast() {
		return tail.getElement();
	}
	
	public int getSize() {
		return size;
	}
			
	public void prepend(E element) {
		Node temp = new Node(element, head, null);
		if(head != null) {
			head.previous = temp;
		}
		head = temp;
		if(tail == null) {
			tail = temp;
		}
		size++;
	}
	
	public void append(E element) {
		Node temp = new Node(element, null, tail);
		if(tail != null) {
			tail.next = temp;
		}
		tail = temp;
		if(head == null) {
			head = temp;
		}
		size++;
	}
		
	public E getHead() {
		return head.getElement();
	}
	
	public void removeLast() {
        if (size == 0) throw new NoSuchElementException();
        tail = tail.previous;
        tail.next = null;
        size--;
    }
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * This is a node containing an element used in a doubly linked list
	 * 
	 *
	 * @param <E> type parameter
	 */
	private class Node {
		private E element;
		private Node next;
		private Node previous;
		/**
		 * Creates a node with a single element, next referring to the next node
		 * @param element
		 * @param next
		 */
		public Node(E element, Node next, Node previous) {
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		
		/**
		 * Getter for the element
		 *  
		 * @return element
		 */
		public E getElement() {
			return element;
		}
		
		/**
		 * Getter for the next node
		 * 
		 * @return next node
		 */
		public Node getNext() {
			return next;
		}
	}
	
	public Iterator<E> iterator() {
		return new DoublyLinkedListIterator();
	}
	
	private class DoublyLinkedListIterator implements  Iterator<E> {
		private Node cursor = head;
		
		/**
		 * Mogelijk alternatief?
		 * 
		@Override
		public boolean hasNext() {
			return !(iterator().next() == null);
		}
		*/
		
		@Override
		public boolean hasNext() {
			 return cursor != null;
		}

		@Override
		public E next() {
			E element = cursor.getElement();
			cursor = cursor.getNext();
			return element;
		}
	}
}
