package snakepong;

import java.awt.Color;
import java.awt.Graphics;

public class Snake {
	
	DoublyLinkedList<Point> snakePoints;
	int xDir, yDir;
	boolean elongate;
	final int STARTSIZE = 21, STARTX = 250, STARTY = 150;
	
	public Snake() {
		snakePoints = new DoublyLinkedList<Point>();
		xDir = 1;
		yDir = 0;
		elongate = false;
		snakePoints.append(new Point(STARTX, STARTY));
		for(int i = 1; i < STARTSIZE; i++) {
			snakePoints.append(new Point(STARTX - i * 4, STARTY));
		}
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.orange);
		for(Point p : snakePoints) {
			g.fillRect(p.getX() - 10 , p.getY() - 10, 20, 20);
		}
	}
	
	public void move() {	
		Point temp = snakePoints.getHead();
		snakePoints.prepend(new Point(temp.getX() + xDir * 4, temp.getY() + yDir * 4));
		snakePoints.removeLast();
	}
	
	public boolean snakeCollision() {
		int x = this.getX();
		int y = this.getY();
		DoublyLinkedList<Point> tail = snakePoints.getTail();
		for(int i = 0; i < 8; i++) {
		tail = tail.getTail();
		}
		for(Point p : tail) {
			if(p.getX() + 20 > x && p.getX() - 20 < x && p.getY() + 20 > y && p.getY() - 20 < y) {
				return true;
			}
		}
		return false;
	}
	
	public int getXdir() {
		return xDir;
	}
	
	public int getYdir() {
		return yDir;
	}
	
	public void setXDir(int x) {
		xDir = x;
	}
	
	public void setYDir(int y) {
		yDir = y;
	}
	
	public int getX() {
		return snakePoints.getHead().getX();
	}
	
	public int getY() {
		return snakePoints.getHead().getY();
	}
	
	
}
